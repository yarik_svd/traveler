from rest_framework import serializers

from users.models import CustomUser

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = CustomUser
        fields = ('first_name', 'last_name', 'username', 'email', 'created_at')


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()
    
    def validate(self, data):
        user = authenticate(username=data.get('email'), password=data.get('password'))

        if not user:
            raise serializers.ValidationError('Incorrect email or password')

        if not user.is_active:
            raise serializers.ValidationError('User is disabled')

        return {'user': user}

class SignUpSerializer(serializers.ModelSerializer):
    password2 = serializers.CharField(max_length=128, write_only=True)

    class Meta:
        model = CustomUser
        fields = ('first_name', 'last_name', 'username', 'email', 'password', 'password2')

    def create(self, validated_data):
        validated_data.pop('password2')
        return CustomUser.objects.create_user(**validated_data)

    def validate(self, data):
        validated_data = super().validate(data)

        if validated_data.get('password') != validated_data.get('password2'):
            raise serializers.ValidationError('Password don\'t match')
        
        return validated_data
