from django.contrib.auth import login, logout
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from rest_framework import views, generics, permissions, authentication
from rest_framework.response import Response
from rest_framework import status

from users.serializers import UserSerializer, LoginSerializer, SignUpSerializer
from users.models import CustomUser
from users.tasks import send_email


class UsersListView(generics.ListAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated,]


class LoginView(views.APIView):
    permission_classes = (permissions.AllowAny,)
    authentication_classes = (authentication.SessionAuthentication,)

    def post(self, request, *args, **kwargs):
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        login(request, user)
        return Response({'detail': 'Successfully logged in.'})


class LogoutView(views.APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        logout(request)
        return Response({'detail': 'Succesfully logged out.'})


class SignUpView(views.APIView):
    permission_classes = (permissions.AllowAny, )

    def post(self, request):
        serializer = SignUpSerializer(data=request.data)
        if serializer.is_valid():
            serializer.validated_data['is_active'] = False
            user = serializer.save()
            current_site = get_current_site(request)
            mail_subject = 'Verify your account'
            to_email = user.email
            send_email.delay((to_email,), mail_subject, 'users/verification_mail', {
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': default_token_generator.make_token(user),
                'domain': current_site.domain,
                'username': user.username
            })
            return Response({'detail': 'Please verify your account. Verification link has been sent to your email.'})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class VerifyUserView(views.APIView):
    '''
        This view is intented to verify user account. If token and uidb64 fetched from verification link is valid, then
        user's 'is_active' attribute set to True and user is allowed to log in.
    '''
    
    def get(self, request, uidb64, token):
        try:
            uid = urlsafe_base64_decode(uidb64).decode()
            user = CustomUser.objects.get(id=uid)
        except(TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        if user is not None and default_token_generator.check_token(user, token):
            user.is_active = True
            user.save()
            return Response({'detail': 'Account successfully verified.'})
        else:
            return Response({'detail': 'Activation link is invalid.'}, status=status.HTTP_400_BAD_REQUEST)           
