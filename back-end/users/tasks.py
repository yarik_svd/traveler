from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from django.conf import settings
from django.template.loader import render_to_string

from traveler.celery import app as celery_app


@celery_app.task
def send_email(recipient, subject, template, contexts):
    
    mail = Mail(
        from_email='traveleryariksvd-test@mail.com',
        to_emails=recipient,
        subject=subject,
        html_content=render_to_string(f'{template}.html', context)
    )
    sg = SendGridAPIClient(settings.SENDGRID_API_KEY)
    sg.send(mail)
