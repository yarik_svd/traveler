from django.urls import path

from users import views

urlpatterns = [
    path('', views.UsersListView.as_view(), name='users_list'),
    path('login', views.LoginView.as_view(), name='login'),
    path('logout', views.LogoutView.as_view(), name='logout'),
    path('signup', views.SignUpView.as_view(), name='signup'),
    path('verify/<uidb64>/<token>', views.VerifyUserView.as_view(), name='verify'),
]

