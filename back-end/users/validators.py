from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
import re

def password_validator(value):
    if not re.match(r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&_-])[A-Za-z\d@$!%*?&_-]{6,}$', value):
        raise ValidationError(
            _(' '.join(['Your password must contain at least 6 characters.', 
                'Also it should contain at least 1 uppercase,', 
                'lowercase and special character @$!%*?&_-. '
            ])
        )
    )
